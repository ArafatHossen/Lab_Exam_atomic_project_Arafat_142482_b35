<?php
namespace App\BITM\SEIP142482\BookTitle;
use App\BITM\SEIP142482\Message\Message;
use App\BITM\SEIP142482\Utility\Utility;
use App\BITM\SEIP142482\Model\Database as DB;
use PDO;

class BookTitle extends DB{
    public $id="";
    public $book_title="";
    public $author_name="";



    public function __construct(){
        parent::__construct();
    }


        public function index(){
            $DBH=$this->connection;
            $sth=$DBH->prepare("select * from book_title");
            $sth->execute();
            $sth->setFetchMode(PDO::FETCH_ASSOC);
            $all_books=array();

            while($row=$sth->fetch()){
                $all_books[]=$row;
            }
            return  $all_books;
        }




    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('book_title',$data)){
            $this->book_title=$data['book_title'];

        }
        if(array_key_exists('author_name',$data)){
            $this->author_name=$data['author_name'];

        }
    }
    public function store(){
       $dbh=$this->connection;
        $values=array($this->book_title,$this->author_name);
        $query="insert into book_title(book_title,author_name) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ BookTitle: $this->book_title ] ,
               [ AuthorName: $this->author_name ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('create.php');



    }


}

