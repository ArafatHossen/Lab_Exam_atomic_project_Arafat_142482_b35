<?php
namespace App\BITM\SEIP142482\Gender;
use App\BITM\SEIP142482\Message\Message;
use App\BITM\SEIP142482\Utility\Utility;
use App\BITM\SEIP142482\Model\Database as DB;
use PDO;

class Gender extends DB{
    public $id="";
    public $book_title="";
    public $author_name="";



    public function __construct(){
        parent::__construct();
    }


        public function index(){
            $DBH=$this->connection;
            $sth=$DBH->prepare("select * from Gender");
            $sth->execute();
            $sth->setFetchMode(PDO::FETCH_ASSOC);
            $all_entry=array();

            while($row=$sth->fetch()){
                $all_entry[]=$row;
            }
            return  $all_entry;
        }




    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];

        }
        if(array_key_exists('gender',$data)){
            $this->gender=$data['gender'];

        }
    }
    public function store(){
       $dbh=$this->connection;
        $values=array($this->name,$this->gender);
        $query="insert into gender(name,gender) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ gender: $this->gender ]
<br> Data Has Been Inserted Successfully!</h3></div>");
        else         Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ gender: $this->gender ]
<br> Data Hasn't Been Inserted Successfully!</h3></div>");



        Utility::redirect('create.php');



    }


}

