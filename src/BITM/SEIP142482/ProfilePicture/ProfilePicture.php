<?php
namespace App\BITM\SEIP142482\ProfilePicture;
use App\BITM\SEIP142482\Message\Message;
use App\BITM\SEIP142482\Utility\Utility;
use App\BITM\SEIP142482\Model\Database as DB;
use PDO;

class ProfilePicture extends DB{
    public $id="";
    public $name="";
    public $picture_address="";



    public function __construct(){
        parent::__construct();
    }


    public function index(){
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from profile_picture");
        $sth->execute();
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $all_picture=array();

        while($row=$sth->fetch()){
            $all_picture[]=$row;
        }
        return  $all_picture;
    }




    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];

        }
        if(array_key_exists('picture_address',$data)){
            $this->picture_address=$data['picture_address'];

        }
    }
    public function store(){
        $dbh=$this->connection;
        $values=array($this->name,$this->picture_address);
        $query="insert into profile_picture(name,picture_address) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ Name: $this->name ] ,
               [ profile picture: $this->picture_address ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('create.php');



    }


}

