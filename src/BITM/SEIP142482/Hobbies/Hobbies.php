<?php
namespace App\BITM\SEIP142482\Hobbies;
use App\BITM\SEIP142482\Message\Message;
use App\BITM\SEIP142482\Utility\Utility;
use App\BITM\SEIP142482\Model\Database as DB;
use PDO;

class Hobbies extends DB{
    public $id="";
    public $name="";
    public $hobbies="";



    public function __construct(){
        parent::__construct();
    }


        public function index(){
            $DBH=$this->connection;
            $sth=$DBH->prepare("select * from hobbies");
            $sth->execute();
            $sth->setFetchMode(PDO::FETCH_ASSOC);
            $all_hobbies=array();

            while($row=$sth->fetch()){
                $all_hobbies[]=$row;
            }
            return  $all_hobbies;
        }




    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];

        }
        if(array_key_exists('hobbies',$data)){
            $this->hobbies=$data['hobbies'];

        }
    }
    public function store(){
       $dbh=$this->connection;
        $values=array($this->name,$this->hobbies);
        $query="insert into hobbies(name,hobbies) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [Hobbies: $this->hobbies ] <br> Data Has Been Inserted Successfully!</h3></div>");
else
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [Hobbies: $this->hobbies ] <br> Data Hasn't Been Inserted Successfully!</h3></div>");

        Utility::redirect('create.php');



    }


}

