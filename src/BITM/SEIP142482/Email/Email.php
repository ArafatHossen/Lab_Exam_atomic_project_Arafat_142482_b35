<?php
namespace App\BITM\SEIP142482\Email;
use App\BITM\SEIP142482\Message\Message;
use App\BITM\SEIP142482\Utility\Utility;
use App\BITM\SEIP142482\Model\Database as DB;
use PDO;

class Email extends DB{
    public $id="";
    public $name="";
    public $email="";



    public function __construct(){
        parent::__construct();
    }


        public function index(){
            $DBH=$this->connection;
            $sth=$DBH->prepare("select * from email");
            $sth->execute();
            $sth->setFetchMode(PDO::FETCH_ASSOC);
            $all_email=array();

            while($row=$sth->fetch()){
                $all_email[]=$row;
            }
            return  $all_email;
        }




    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];

        }
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];

        }
    }
    public function store(){
       $dbh=$this->connection;
        $values=array($this->name,$this->email);
        $query="insert into email(name,email) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] ,
 [ email: $this->email ] <br> Data Has Been Inserted Successfully!</h3></div>");

        else        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] ,
 [ email: $this->email ] <br> Data Hasn't Been Inserted Successfully!</h3></div>");

        Utility::redirect('create.php');



    }


}

