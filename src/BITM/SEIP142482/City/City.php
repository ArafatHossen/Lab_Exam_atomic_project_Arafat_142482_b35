<?php
namespace App\BITM\SEIP142482\City;
use App\BITM\SEIP142482\Message\Message;
use App\BITM\SEIP142482\Utility\Utility;
use App\BITM\SEIP142482\Model\Database as DB;
use PDO;

class City extends DB{
    public $id="";
    public $name="";
    public $city_name="";



    public function __construct(){
        parent::__construct();
    }


        public function index(){
            $DBH=$this->connection;
            $sth=$DBH->prepare("select * from city");
            $sth->execute();
            $sth->setFetchMode(PDO::FETCH_ASSOC);
            $all_city=array();

            while($row=$sth->fetch()){
                $all_city[]=$row;
            }
            return  $all_city;
        }




    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];

        }
        if(array_key_exists('city_name',$data)){
            $this->city_name=$data['city_name'];

        }
    }
    public function store(){
       $dbh=$this->connection;
        $values=array($this->name,$this->city_name);
        $query="insert into city(name,city_name) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        Message::message("<div id='msg'></div><h3 align='center'>[ name: $this->name ] ,
[  city : $this->city_name ] <br> Data Has Been Inserted Successfully!</h3></div>");
        else
            Message::message("<div id='msg'></div><h3 align='center'>[ name: $this->name ] ,
[  city : $this->city_name ] <br> Data Hasn't Been Inserted Successfully!</h3></div>");



        Utility::redirect('create.php');



    }


}

