<?php
namespace App\BITM\SEIP142482\SummaryOfOrganization;
use App\BITM\SEIP142482\Message\Message;
use App\BITM\SEIP142482\Utility\Utility;
use App\BITM\SEIP142482\Model\Database as DB;
use PDO;

class SummaryOfOrganization extends DB{
    public $id="";
    public $organization_name="";
    public $organization_summary="";



    public function __construct(){
        parent::__construct();
    }


        public function index(){
            $DBH=$this->connection;
            $sth=$DBH->prepare("select * from summary_of_organization");
            $sth->execute();
            $sth->setFetchMode(PDO::FETCH_ASSOC);
            $all_organization=array();

            while($row=$sth->fetch()){
                $all_organization[]=$row;
            }
            return  $all_organization;
        }




    public function setData($data=null){
        
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('organization_name',$data)){
            $this->organization_name=$data['organization_name'];

        }
        if(array_key_exists('organization_summary',$data)){
            $this->organization_summary=$data['organization_summary'];

        }
    }
    public function store(){
       $dbh=$this->connection;
        $values=array($this->organization_name,$this->organization_summary);
        $query="insert into summary_of_organization(organization_name,organization_summary) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        Message::message("<div id='msg'><h3 align='center'>[ Organization name: $this->organization_name ] ,
 [ organization_summary: $this->organization_summary ] <br> Data Has Been Inserted Successfully!</h3></div>");
        else         Message::message("<div id='msg'><h3 align='center'>[ Organization name: $this->organization_name ] ,
 [ organization_summary: $this->organization_summary ] <br> Data Hasn't Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');



    }


}

