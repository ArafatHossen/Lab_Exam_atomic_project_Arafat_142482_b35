<?php
namespace App\BITM\SEIP142482\Birthday;
use App\BITM\SEIP142482\Message\Message;
use App\BITM\SEIP142482\Utility\Utility;
use App\BITM\SEIP142482\Model\Database as DB;
use PDO;

class Birthday extends DB{
    public $id="";
    public $name="";
    public $birthday="";



    public function __construct(){
        parent::__construct();
    }


    public function index(){
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from birthday");
        $sth->execute();
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $all_birthday=array();

        while($row=$sth->fetch()){
            $all_birthday[]=$row;
        }
        return  $all_birthday;
    }




    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];

        }
        if(array_key_exists('birthday',$data)){
            $this->birthday=$data['birthday'];

        }
    }
    public function store(){
        $dbh=$this->connection;
        $values=array($this->name,$this->birthday);
        $query="INSERT INTO birthday(name,birthday) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ Name: $this->name ] ,
               [ birthday: $this->birthday ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('create.php');



    }


}

