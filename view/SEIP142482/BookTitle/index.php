<?php
require_once("../../../vendor/autoload.php");



//use App\BITM\SEIP128778\BookTitle;

$objBookTitle= new \App\BITM\SEIP142482\BookTitle\BookTitle();


$all_books= $objBookTitle->index();

?>
<!--table-->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title> </title>

    <!-- Latest compiled and minified CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
   <!-- <link rel="stylesheet" href="../../../resource/Bootstrap/css/booktitle.css"> -->
</head>

<style>
    .main{
        margin-top: 10%;
        margin-left: 10%;
        margin-right:20%;
        background-color: #679a9f;



    }
    body{
        background-image:url("../../../resource/assets/images/book.jpg");

        background-repeat:no-repeat;
        background-size: 100% 925px;

    }



</style>

<body  >
<div class="container ">

        <div class="main">
            <div class="">
                <div class="table-responsive" id="whole">
                    </br></br></br></br>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>Book title</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <?php
                            $serial=0;

                            foreach($all_books as $book){
                            $serial++; ?>
                            <td><?php echo $serial?></td>
                            <td><?php echo $book['id']?></td>
                            <td><?php echo $book['book_title']?></td>
                            <td><?php echo $book['author_name']?></td>
                            <td><a href="view.php?id=<?php echo $book-> id ?>" class="btn btn-primary" role="button">View</a>
                                <a href="edit.php?id=<?php echo $book-> id ?>"  class="btn btn-info" role="button">Edit</a>
                                <a href="delete.php?id=<?php echo $book->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                                <a href="trash.php?id=<?php echo $book->id ?>"  class="btn btn-info" role="button">Trash</a>
                            </td>

                        </tr>
                        <?php }?>


                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
</div>

<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</div>
</body>
</html>



